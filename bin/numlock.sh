#!/bin/bash

xset q | grep Caps | cut -c 46-48 | tr "[:lower:]" "[:upper:]"
