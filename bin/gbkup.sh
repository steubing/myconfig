#!/bin/bash

echo Backing up Documents to Google Drive...
rclone copy ~/Documents gdrive:Backups/Documents --delete-excluded

echo Backing up Pictures to Google Drive...
rclone copy ~/Pictures gdrive:Backups/Pictures --delete-excluded

echo Backing up Stuff to Google Drive...
rclone copy ~/Stuff gdrive:Backups/Stuff --delete-excluded

# Music
# Video

echo All done.
