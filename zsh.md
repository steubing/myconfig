sudo apt install zsh zsh-autosuggestions zsh-syntax-highlighting
cd git
git clone https://github.com/matmutant/zsh-insulter.git zsh-insulter

At top of  .zshrc:
    source /usr/share/zsh-autosuggestions/zsh-autosuggestions.zsh

Remove compinit

At bottom of .zshrc:
    source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
    source ~/git/zsh-insulter/src/zsh.command-not-found

sudo chsh

git clone --depth=1 https://github.com/romkatv/powerlevel10k.git ~/powerlevel10k
echo 'source ~/powerlevel10k/powerlevel10k.zsh-theme' >>~/.zshrc

At very top of .zshrc:
    source ~/.aliasrc

